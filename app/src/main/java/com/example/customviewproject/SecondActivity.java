package com.example.customviewproject;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.example.views.QuestionView;

public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        QuestionView questionView = findViewById(R.id.questionView3);
        questionView.setText("Have you drive license?");
        questionView.setFirstCheckBox("Yes");
        questionView.setSecondCheckBox("No");
    }
}
