package com.example.views

import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.util.TypedValue
import android.view.View
import android.widget.TextView

class ScaleTextView(context: Context?, attrs: AttributeSet?) : TextView(context, attrs), View.OnClickListener {

    private var startTextSize: Float
    private val DOUBLE_CLICK_TIME_DELTA: Long = 300
    private var currentClickTime: Long = 0

    init {
        setOnClickListener(this)
        startTextSize = textSize
        Log.d("size", textSize.toString())
        Log.d("startsize", startTextSize.toString())
    }

    override fun onClick(v: View?) {

        var clickTime = System.currentTimeMillis()

        if (clickTime - currentClickTime < DOUBLE_CLICK_TIME_DELTA) {

            if (textSize == startTextSize) {
                setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize * 2f)
            } else {
                setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize / 2f)
            }
        }
        currentClickTime = clickTime
    }

}