package com.example.views

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.CheckBox
import android.widget.RelativeLayout
import android.widget.TextView
import com.example.customviewproject.R
import kotlinx.android.synthetic.main.question_view_layout.view.*

class QuestionView(context: Context?, attrs: AttributeSet?) : RelativeLayout(context, attrs) {

    init {
        inflate(context, R.layout.question_view_layout, this)

        val textView: TextView = findViewById(R.id.questionTextView)
        val firstCheckBox: CheckBox = findViewById(R.id.firstCheckBox)
        val secondCheckBox: CheckBox = findViewById(R.id.secondCheckBox)

        val attributes = context!!.obtainStyledAttributes(attrs, R.styleable.QuestionView)
        textView.text = attributes.getString(R.styleable.QuestionView_setText)
        firstCheckBox.text = attributes.getString(R.styleable.QuestionView_setFirstCheckBoxText)
        secondCheckBox.text = attributes.getString(R.styleable.QuestionView_setSecondCheckBoxText)
        attributes.recycle()
    }

    fun setText(text: String) {
        val textView: TextView = findViewById(R.id.questionTextView)
        textView.text = text
    }

    fun setFirstCheckBox(text: String) {
        val firstCheckBox: CheckBox = findViewById(R.id.firstCheckBox)
        firstCheckBox.text = text
    }

    fun setSecondCheckBox(text: String) {
       val secondCheckBox: CheckBox = findViewById(R.id.secondCheckBox)
       secondCheckBox.text = text
    }

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        if (secondCheckBox.text == "") {
            secondCheckBox.visibility = View.GONE
        }
        super.onLayout(changed, l, t, r, b)
    }

}