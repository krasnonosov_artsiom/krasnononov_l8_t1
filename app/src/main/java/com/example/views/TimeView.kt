package com.example.views

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View
import androidx.core.content.ContextCompat
import com.example.customviewproject.R
import java.util.*

class TimeView(context: Context?, attrs: AttributeSet?) : View(context, attrs) {

    private val paint = Paint()
    private var size: Int = 0
    private var textSize: Float = 0f
    private var textSizeDescription = 0f

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        textSize = width / 7 * 0.6f
        textSizeDescription = textSize / 1.5f

        drawSecondsColumn(canvas)
        drawMinutes(canvas)
        drawHours(canvas)
        invalidate()
    }

    private fun drawHours(canvas: Canvas?) {
        val leftX = width / 7.toFloat()
        val rightX = width / 7 * 2.toFloat()
        paint.color = ContextCompat.getColor(context, R.color.backgroundBlack)
        canvas!!.drawRect(leftX, height.toFloat(), rightX, (height - height * 0.8f), paint)
        paint.color = ContextCompat.getColor(context, R.color.black)
        paint.style = Paint.Style.FILL
        val calendar = Calendar.getInstance()
        var hour = (calendar.get(Calendar.HOUR_OF_DAY)).toFloat()
        canvas.drawRect(leftX, height.toFloat(), rightX, (height - height / 24 * hour * 0.8f), paint)
        paint.textSize = textSizeDescription
        val textDescription = "hours"
        canvas.drawText(textDescription, leftX + (width / 7 - paint.measureText(textDescription)) / 2, height * 0.1f, paint)
        paint.textSize = textSize
        paint.color = Color.WHITE
        var text = hour.toInt().toString()
        canvas.drawText(text, leftX + (width / 7 - paint.measureText(text)) / 2, height - (textSize + 5f), paint)
    }

    private fun drawMinutes(canvas: Canvas?) {
        val leftX = width / 7 * 3.toFloat()
        val rightX = width / 7 * 4.toFloat()
        paint.color = ContextCompat.getColor(context, R.color.backgroundBlack)
        canvas!!.drawRect(leftX, height.toFloat(), rightX, (height - height * 0.8f), paint)
        paint.color = Color.BLACK
        paint.style = Paint.Style.FILL
        val calendar = Calendar.getInstance()
        var minutes = calendar.get(Calendar.MINUTE).toFloat()
        canvas.drawRect(leftX, height.toFloat(), rightX, (height - height / 60 * minutes * 0.8f), paint)
        paint.textSize = textSizeDescription
        val textDescription = "minutes"
        canvas.drawText(textDescription, leftX + (width / 7 - paint.measureText(textDescription)) / 2, height * 0.1f, paint)
        paint.textSize = textSize
        paint.color = Color.WHITE
        var text = minutes.toInt().toString()
        canvas.drawText(text, leftX + (width / 7 - paint.measureText(text)) / 2, height - (textSize + 5f), paint)

    }

    private fun drawSecondsColumn(canvas: Canvas?) {
        val leftX = width / 7 * 5.toFloat()
        val rightX = width / 7 * 6.toFloat()
        paint.color = ContextCompat.getColor(context, R.color.backgroundBlack)
        canvas!!.drawRect(leftX, height.toFloat(), rightX, (height - height.toFloat() * 0.8f), paint)
        paint.color = Color.BLACK
        paint.style = Paint.Style.FILL
        val calendar = Calendar.getInstance()
        var seconds = calendar.get(Calendar.SECOND)
        canvas.drawRect(leftX, height.toFloat(), rightX, (height - height.toFloat() / 60 * seconds * 0.8f), paint)
        paint.textSize = textSizeDescription
        val textDescription = "seconds"
        canvas.drawText(textDescription, leftX + (width / 7 - paint.measureText(textDescription)) / 2, height * 0.1f, paint)
        paint.textSize = textSize
        paint.color = Color.WHITE
        var text = seconds.toString()
        canvas.drawText(text, leftX + (width / 7 - paint.measureText(text)) / 2, height - (textSize + 5f), paint)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {

        super.onMeasure(widthMeasureSpec, heightMeasureSpec)

        size = Math.min(measuredWidth, measuredHeight)
        setMeasuredDimension(size, size)
    }
}
